package com.vitoesposito;

import java.sql.SQLException;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class WebServerEndPoints {
	static final boolean shouldPrintPosts = true;
	static final boolean shouldPrintResults = true;

	private static boolean hasAllKeys(JSONObject post, String... keys) {
		for (String key : keys) {
			if (!post.has(key)) {
				return false;
			}
		}
		return true;
	}

	public static JSONObject login(JSONObject post, boolean getResponse) throws JSONException, SQLException {
		if (shouldPrintPosts) {
			System.out.println("URL: login");
			System.out.println(post.toString(2));
		}

		SQLLiteDB db = SQLLiteDB.get();
		JSONObject result = new JSONObject();
		if (hasAllKeys(post, JK.KEY_USER_ID, JK.KEY_USER_FACEBOOK_TOKEN)) {
			if (db.isUserIDTokenValid(post.getInt(JK.KEY_USER_ID), post.getString(JK.KEY_USER_FACEBOOK_TOKEN))) {
				result = null;
			} else {
				result.put(JK.WEB_STATUS, "Invalid Id: \"" + post.getInt(JK.KEY_USER_ID) + "\"");
			}
		} else if (hasAllKeys(post, JK.KEY_USER_NAME, JK.KEY_USER_FACEBOOK_TOKEN)) {
			int userId = db.getUserIDFromUserNameFB(post.getString(JK.KEY_USER_NAME),
					post.getString(JK.KEY_USER_FACEBOOK_TOKEN));
			if (userId != -1) {
				if (getResponse) {
					System.out.println("Login: " + post.getString(JK.KEY_USER_NAME));
					result.put(JK.WEB_STATUS, "SUCCESS");
					result.put(JK.KEY_USER_ID, userId);
				} else {
					// return null if successful
					result = null;
				}

			} else {
				// db.user
				result.put(JK.WEB_STATUS, "Invalid User: \"" + post.getString(JK.KEY_USER_NAME) + "\"");
			}
		} else {
			result.put(JK.WEB_STATUS, "Invalid Request");
		}

		if (shouldPrintResults) {
			System.out.println("Result: login");
			if (result == null) {
				System.out.println("Valid User");
			} else {
				System.out.println(result.toString(2));
			}
		}
		return result;
	}

	public static JSONObject getQuizzes(JSONObject post) throws JSONException, SQLException {
		if (shouldPrintPosts) {
			System.out.println("URL: quizzes");
			System.out.println(post.toString(2));
		}
		SQLLiteDB db = SQLLiteDB.get();
		JSONObject result = new JSONObject();
		if (!hasAllKeys(post, JK.KEY_USER_ID, JK.KEY_USER_FACEBOOK_TOKEN, JK.WEB_QUIZ_COMPLETED)) {
			return null;
		}

		Vector<Integer> completedIds = db.getQuizIDsCompleted(post.getInt(JK.KEY_USER_ID));

		boolean isCompleted = post.getBoolean(JK.WEB_QUIZ_COMPLETED);
		Vector<Integer> results;
		if (post.has(JK.WEB_QUIZ_CREATOR)) {
			results = db.getQuizByCreator(post.getInt(JK.WEB_QUIZ_CREATOR));
		} else {
			results = db.getAllQuizzes();
		}

		JSONArray gameArray = new JSONArray();
		for (int id : results) {
			if (isCompleted) {
				if (completedIds.contains(id)) {
					JSONObject json = db.getQuizData(id, false);
					gameArray.put(json);
				}
			} else {
				if (!completedIds.contains(id)) {
					JSONObject json = db.getQuizData(id, false);
					gameArray.put(json);
				}
			}
		}
		result.put(JK.WEB_GAME_LIST, gameArray);

		if (shouldPrintResults) {
			System.out.println("Result: quizzes");
			System.out.println(result.toString(2));
		}
		return result;
	}

	public static JSONObject getQuiz(JSONObject post) throws JSONException, SQLException {
		if (shouldPrintPosts) {
			System.out.println("URL: quiz");
			System.out.println(post.toString(2));
		}
		SQLLiteDB db = SQLLiteDB.get();
		JSONObject result = new JSONObject();
		if (!hasAllKeys(post, JK.KEY_USER_ID, JK.KEY_USER_FACEBOOK_TOKEN, JK.KEY_QUIZ_ID)) {
			return null;
		}

		result = db.getQuizData(post.getInt(JK.KEY_QUIZ_ID), true);

		if (shouldPrintResults) {
			System.out.println("Result: quiz");
			System.out.println(result.toString(2));
		}
		return result;
	}

	public static JSONObject putResults(JSONObject post) {
		if (shouldPrintPosts) {
			System.out.println("URL: results");
			System.out.println(post.toString(2));
		}
		SQLLiteDB db = SQLLiteDB.get();
		JSONObject result = new JSONObject();
		if (!hasAllKeys(post, JK.KEY_USER_ID, JK.KEY_USER_FACEBOOK_TOKEN, JK.KEY_QUIZ_ID, JK.KEY_QUIZ_ANSWERS,
				JK.KEY_QUIZ_DURATIONS)) {
			return null;
		}

		result = db.addQuizResults(post);

		if (shouldPrintResults) {
			System.out.println("Result: result");
			System.out.println(result.toString(2));
		}
		return result;
	}

	public static JSONObject putReview(JSONObject post) {
		if (shouldPrintPosts) {
			System.out.println("URL: review");
			System.out.println(post.toString(2));
		}
		SQLLiteDB db = SQLLiteDB.get();
		JSONObject result = new JSONObject();
		if (!hasAllKeys(post, JK.KEY_USER_ID, JK.KEY_USER_FACEBOOK_TOKEN, JK.KEY_QUIZ_ID, JK.KEY_DIFFICULTY,
				JK.KEY_FUN_FACTOR, JK.KEY_RELEVANCE, JK.KEY_FLAGGED_COUNT)) {
			return null;
		}

		result = db.addQuizReview(post);

		if (shouldPrintResults) {
			System.out.println("Result: review");
			System.out.println(result.toString(2));
		}
		return result;
	}

	public static JSONObject addQuiz(JSONObject post) {
		if (shouldPrintPosts) {
			System.out.println("URL: addQuiz");
			System.out.println(post.toString(2));
		}
		SQLLiteDB db = SQLLiteDB.get();
		JSONObject result = new JSONObject();
		if (!hasAllKeys(post, JK.KEY_USER_ID, JK.KEY_QUIZ_QUESTIONS, JK.KEY_QUIZ_TITLE, JK.KEY_QUIZ_ALLOW_RANDOM)) {
			return null;
		}

		post.put(JK.KEY_QUIZ_CREATOR_ID, post.getInt(JK.KEY_USER_ID));
		post.put(JK.KEY_DIFFICULTY, -1);
		post.put(JK.KEY_FUN_FACTOR, -1);
		post.put(JK.KEY_RELEVANCE, -1);
		post.put(JK.KEY_FLAGGED_COUNT, 0);
		result = db.updateQuizData(-1, post);

		if (shouldPrintResults) {
			System.out.println("Result: addQuiz");
			System.out.println(result.toString(2));
		}
		return result;
	}
}
