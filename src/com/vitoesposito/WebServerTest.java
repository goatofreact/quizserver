package com.vitoesposito;

import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

public class WebServerTest {

	public static void main(String[] args) throws JSONException, SQLException {
		SQLLiteDB db = SQLLiteDB.get();
		db.reset();
		SQLLiteDBgenerate.generateTestData();

		{
			System.out.println();
			String user = "Vito";
			String fbToken = "tokenVito";
			JSONObject post = new JSONObject();
			post.put(JK.KEY_USER_NAME, user);
			post.put(JK.KEY_USER_FACEBOOK_TOKEN, fbToken);

			JSONObject response = WebServerEndPoints.login(post, false);
			if (response != null) {
				System.out.println("Fail No User ID: \"" + user + "\"");
				System.out.println("Fail No User FB: \"" + fbToken + "\"");
				System.exit(-1);
			}
		}

		{
			System.out.println();
			JSONObject post = new JSONObject();
			post.put(JK.KEY_USER_NAME, "Not a user");
			post.put(JK.KEY_USER_FACEBOOK_TOKEN, "Not a user");

			JSONObject response = WebServerEndPoints.login(post, false);
			if (response == null || !response.has(JK.WEB_STATUS)) {
				System.out.println("Didn't fail");
				System.exit(-1);
			}
		}

		{
			System.out.println();
			System.out.println("Quizes all completed");
			JSONObject post = new JSONObject();
			post.put(JK.KEY_USER_ID, db.getUserIDFromUserName("Vito"));
			post.put(JK.KEY_USER_FACEBOOK_TOKEN, "tokenVito");
			post.put(JK.WEB_QUIZ_COMPLETED, true);
			JSONObject response = WebServerEndPoints.getQuizzes(post);

			System.out.println(response.toString(2));
		}
		{
			System.out.println();
			System.out.println("Quizes all not completed");
			JSONObject post = new JSONObject();
			post.put(JK.KEY_USER_ID, db.getUserIDFromUserName("Vito"));
			post.put(JK.KEY_USER_FACEBOOK_TOKEN, "tokenVito");
			post.put(JK.WEB_QUIZ_COMPLETED, false);
			JSONObject response = WebServerEndPoints.getQuizzes(post);

			System.out.println(response.toString(2));
		}
		{
			System.out.println();
			System.out.println("Quizzes by 1 completed");
			JSONObject post = new JSONObject();
			post.put(JK.KEY_USER_ID, db.getUserIDFromUserName("Vito"));
			post.put(JK.KEY_USER_FACEBOOK_TOKEN, "tokenVito");
			post.put(JK.WEB_QUIZ_CREATOR, 1);
			post.put(JK.WEB_QUIZ_COMPLETED, true);
			JSONObject response = WebServerEndPoints.getQuizzes(post);

			System.out.println(response.toString(2));
		}
		{
			System.out.println();
			System.out.println("Quizzes by 1 not completed");
			JSONObject post = new JSONObject();
			post.put(JK.KEY_USER_ID, db.getUserIDFromUserName("Vito"));
			post.put(JK.KEY_USER_FACEBOOK_TOKEN, "tokenVito");
			post.put(JK.WEB_QUIZ_CREATOR, 1);
			post.put(JK.WEB_QUIZ_COMPLETED, false);
			JSONObject response = WebServerEndPoints.getQuizzes(post);

			System.out.println(response.toString(2));
		}
		int quizId = 2;
		{
			System.out.println();
			System.out.println("Quiz 2");
			JSONObject post = new JSONObject();
			post.put(JK.KEY_USER_ID, db.getUserIDFromUserName("Vito"));
			post.put(JK.KEY_USER_FACEBOOK_TOKEN, "tokenVito");
			post.put(JK.KEY_QUIZ_ID, quizId);
			JSONObject response = WebServerEndPoints.getQuiz(post);

			System.out.println(response.toString(2));
		}

		{
			System.out.println();
			System.out.println("Quiz 2 Results");
			JSONObject post = SQLLiteDBgenerate.generateQuizResults(quizId, 0, 1, 2, 3, 0, 1, 2, 3, 2, 2);
			post.put(JK.KEY_USER_ID, db.getUserIDFromUserName("Lonnie"));
			post.put(JK.KEY_USER_FACEBOOK_TOKEN, "tokenVito");

			JSONObject response = WebServerEndPoints.putResults(post);

			System.out.println(response.toString(2));
		}
		{
			System.out.println();
			System.out.println("Quiz 2 Review");
			JSONObject post = SQLLiteDBgenerate.generateQuizReview(quizId, 1, 3, 2, 0);
			post.put(JK.KEY_USER_ID, db.getUserIDFromUserName("Lonnie"));
			post.put(JK.KEY_USER_FACEBOOK_TOKEN, "tokenVito");

			JSONObject response = WebServerEndPoints.putReview(post);

			System.out.println(response.toString(2));
		}

		
		
		
		db.close();
		System.out.println("Success");
	}

}
