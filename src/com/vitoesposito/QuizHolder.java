package com.vitoesposito;

import org.json.JSONArray;
import org.json.JSONObject;

public class QuizHolder extends JSONObject {
	JSONObject json;
	int totalResults = 0;
	int totalDuration = 0;
	int totalCorrect = 0;
	int[] durationPerQuestion = new int[10];
	int[] correctCount = new int[10];
	int[] wrong1Count = new int[10];
	int[] wrong2Count = new int[10];
	int[] wrong3Count = new int[10];

	int difficulty = 0;
	int difficultyCount = 0;
	int relevance = 0;
	int relevanceCount = 0;
	int funFactor = 0;
	int funFactorCount = 0;
	int totalFlagged = 0;

	public QuizHolder(JSONObject quizData) {
		json = quizData;
		System.out.println("Quiz");
		System.out.println(quizData.toString(2));
		for (int i = 0; i < durationPerQuestion.length; i++) {
			durationPerQuestion[i] = 0;
			correctCount[i] = 0;
			wrong1Count[i] = 0;
			wrong2Count[i] = 0;
			wrong3Count[i] = 0;
		}
	}

	public void addResult(JSONObject result) {
		{
			JSONArray quizDurations = result.getJSONArray(JK.KEY_QUIZ_DURATIONS);
			int localTotal = 0;
			for (int i = 0; i < quizDurations.length(); i++) {
				int current = quizDurations.getInt(i);
				localTotal += current;
				durationPerQuestion[i] += current;
			}
			totalDuration += localTotal;

		}
		{
			JSONArray quizAnswers = result.getJSONArray(JK.KEY_QUIZ_ANSWERS);
			totalResults++;

			for (int i = 0; i < quizAnswers.length(); i++) {
				switch (quizAnswers.getInt(i)) {
				default:
				case 0:
					totalCorrect++;
					correctCount[i]++;
					break;
				case 1:
					wrong1Count[i]++;
					break;
				case 2:
					wrong2Count[i]++;
					break;
				case 3:
					wrong3Count[i]++;
					break;
				}

			}

		}
	}

	public void addReview(JSONObject review) {
		System.out.println(review.toString(2));
		int tempDifficulty = review.getInt(JK.KEY_DIFFICULTY);
		int tempFunFactor = review.getInt(JK.KEY_FUN_FACTOR);
		int tempRelevance = review.getInt(JK.KEY_RELEVANCE);
		int tempFlagged = review.getInt(JK.KEY_FLAGGED_COUNT);

		if (tempDifficulty != -1) {
			difficultyCount++;
			difficulty += tempDifficulty;
		}
		if (tempRelevance != -1) {
			relevanceCount++;
			relevance += tempRelevance;
		}
		if (tempFunFactor != -1) {
			funFactorCount++;
			funFactor += tempFunFactor;
		}
		if (tempFlagged != -1) {
			totalFlagged += tempFlagged;
		}
	}

	public JSONObject getJSON() {
		if (difficultyCount != 0) {
			json.put(JK.KEY_DIFFICULTY, difficulty / difficultyCount);
		}
		if (relevanceCount != 0) {
			json.put(JK.KEY_RELEVANCE, relevance / relevanceCount);
		}
		if (funFactorCount != 0) {
			json.put(JK.KEY_FUN_FACTOR, funFactor / funFactorCount);
		}
		if (totalFlagged != 0) {
			json.put(JK.KEY_FLAGGED_COUNT, totalFlagged);
		}

		return json;
	}

}
