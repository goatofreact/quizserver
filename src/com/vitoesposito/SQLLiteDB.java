package com.vitoesposito;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import org.json.JSONException;
import org.json.JSONObject;

public class SQLLiteDB {
	String fileName = "data.db";
	Connection conn = null;
	static SQLLiteDB db;
	Object lock = new Object();

	static SQLLiteDB get() {
		if (db == null) {
			db = new SQLLiteDB();
		}
		return db;
	}

	private SQLLiteDB() {
		try {
			String url = "jdbc:sqlite:." + File.separator + fileName;
			conn = DriverManager.getConnection(url);
			System.out.println("Connection to SQLite has been established.");
			{
				String usersTable = "CREATE TABLE IF NOT EXISTS users ( id integer PRIMARY KEY, name text UNIQUE, facebookToken text, data text);";
				Statement stmt = conn.createStatement();
				stmt.execute(usersTable);
			}
			{
				String quizzesTable = "CREATE TABLE IF NOT EXISTS quizzes ( id integer PRIMARY KEY, creator integer, data text);";

				Statement stmt = conn.createStatement();
				stmt.execute(quizzesTable);
			}
			{
				String quizResultsTable = "CREATE TABLE IF NOT EXISTS quizResults ( id integer PRIMARY KEY, quizId integer, userId integer, results text, review text);";
				Statement stmt = conn.createStatement();
				stmt.execute(quizResultsTable);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.exit(-1);
		}
	}

	void updateUserData(int userId, JSONObject data) {
		synchronized (lock) {
			try {
				String sql;
				PreparedStatement pstmt;
				if (userId != -1) {
					sql = "UPDATE users SET name = ?, data = ? WHERE id = ?;";
					pstmt = conn.prepareStatement(sql);
					pstmt.setString(1, data.getString(JK.KEY_USER_NAME));
					pstmt.setString(2, data.toString());
					pstmt.setInt(3, userId);
				} else {
					sql = "INSERT INTO users(name, facebookToken,data) VALUES(?,?,?)";
					pstmt = conn.prepareStatement(sql);
					pstmt.setString(1, data.getString(JK.KEY_USER_NAME));
					pstmt.setString(2, data.getString(JK.KEY_USER_FACEBOOK_TOKEN));
					pstmt.setString(3, data.toString(2));
				}
				pstmt.executeUpdate();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				System.exit(-1);
			}
		}
	}

	JSONObject updateQuizData(int quizId, JSONObject data) throws JSONException {
		synchronized (lock) {
			JSONObject result = new JSONObject();
			try {
				String sql;
				PreparedStatement pstmt;
				if (quizId != -1) {
					sql = "UPDATE quizzes SET data = ? WHERE id = ?;";
					pstmt = conn.prepareStatement(sql);
					pstmt.setString(1, data.toString(2));
					pstmt.setInt(2, quizId);
					result.put(JK.WEB_ACTION, "Update");
				} else {
					sql = "INSERT INTO quizzes(creator, data) VALUES(?,?)";
					pstmt = conn.prepareStatement(sql);
					pstmt.setInt(1, data.getInt(JK.KEY_QUIZ_CREATOR_ID));
					pstmt.setString(2, data.toString(2));
					result.put(JK.WEB_ACTION, "Added");
				}
				pstmt.executeUpdate();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				System.exit(-1);
			}
			return result;
		}
	}

	public int getUserIDFromUserName(String userName) throws SQLException {
		synchronized (lock) {
			String sql = "SELECT id FROM users WHERE name = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, userName);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				return rs.getInt("id");
			} else {
				// TODO add to an error log
				// System.out.println("Can't find UserName: " + userName);
				return -1;
			}
		}
	}

	public int getUserIDFromUserNameFB(String userName, String facebookToken) throws SQLException {
		synchronized (lock) {
			String sql = "SELECT id FROM users WHERE name = ? AND facebookToken = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, userName);
			pstmt.setString(2, facebookToken);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				return rs.getInt("id");
			} else {
				// TODO add to an error log
				// System.out.println("Can't find FacebookToken: " +
				// facebookToken);
				return -1;
			}
		}
	}

	public int getQuizID(String quizName) throws SQLException {
		synchronized (lock) {
			String sql = "SELECT id FROM quizzes WHERE title = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, quizName);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				return rs.getInt("id");
			} else {
				System.out.println("Can't find QuizName: " + quizName);
				return -1;
			}
		}
	}

	public JSONObject getUserData(int userID) throws SQLException {
		synchronized (lock) {
			String sql = "SELECT data FROM users WHERE id = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, userID);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				String temp = rs.getString("data");
				JSONObject json = new JSONObject(temp);
				if (json.getInt(JK.KEY_USER_ID) == -1) {
					// code to add user id after the insert
					// will be saved on next save so no big deal
					json.put(JK.KEY_USER_ID, userID);
				}
				return json;
			} else {
				System.out.println("Can't find UserID: " + userID);
				return null;
			}
		}
	}

	public JSONObject getUserStats(int userID) throws SQLException {
		synchronized (lock) {
			String sql = "SELECT data FROM users WHERE id = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, userID);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				String temp = rs.getString("data");
				JSONObject json = new JSONObject(temp);
				return json.getJSONObject(JK.KEY_USER_STATS);
			} else {
				System.out.println("Can't find UserID: " + userID);
				return null;
			}
		}
	}

	public JSONObject getQuizData(int quizId, boolean getWholeQuiz) throws SQLException {
		synchronized (lock) {
			String sql = "SELECT data FROM quizzes WHERE id = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, quizId);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				String temp = rs.getString("data");
				JSONObject json = new JSONObject(temp);
				if (json.getInt(JK.KEY_QUIZ_ID) == -1) {
					// code to add user id after the insert
					// will be saved on next save so no big deal
					json.put(JK.KEY_QUIZ_ID, quizId);
				}
				if (!getWholeQuiz) {
					json.remove(JK.KEY_QUIZ_QUESTIONS);
					json.remove(JK.KEY_QUIZ_ALLOW_RANDOM);
				}
				return json;
			}
			return null;
		}
	}

	public void reset() throws SQLException {
		synchronized (lock) {
			{
				String sql = "DELETE FROM users;";
				PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.executeUpdate();
			}
			{
				String sql = "DELETE FROM quizzes;";
				PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.executeUpdate();
			}
			{
				String sql = "DELETE FROM quizResults;";
				PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.executeUpdate();
			}
		}
	}

	public void close() throws SQLException {
		synchronized (lock) {
			if (conn != null) {
				conn.close();
				conn = null;
			}
		}
	}

	public Vector<Integer> getQuizIDsCompleted(int userId) throws SQLException {
		synchronized (lock) {
			Vector<Integer> quizIds = new Vector<Integer>();
			String sql = "SELECT quizId FROM quizResults WHERE userId = ?;";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, userId);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				quizIds.add(rs.getInt("quizId"));
			}
			return quizIds;
		}
	}

	public Vector<Integer> getQuizByCreator(int creator) throws SQLException {
		synchronized (lock) {
			Vector<Integer> quizIds = new Vector<Integer>();
			String sql = "SELECT id FROM quizzes WHERE creator = ?;";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, creator);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				quizIds.add(rs.getInt("id"));
			}
			return quizIds;
		}
	}

	public Vector<Integer> getAllQuizzes() throws SQLException {
		synchronized (lock) {
			Vector<Integer> quizIds = new Vector<Integer>();
			String sql = "SELECT id FROM quizzes;";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				quizIds.add(rs.getInt("id"));
			}
			return quizIds;
		}
	}

	public JSONObject addQuizResults(JSONObject json) {
		synchronized (lock) {
			try {
				String sql;
				PreparedStatement pstmt;
				sql = "INSERT INTO quizResults(quizId, userId, results) VALUES(?,?,?)";
				pstmt = conn.prepareStatement(sql);
				int quizID = json.getInt(JK.KEY_QUIZ_ID);
				pstmt.setInt(1, quizID);
				pstmt.setInt(2, json.getInt(JK.KEY_USER_ID));
				json.remove(JK.KEY_QUIZ_ID);
				json.remove(JK.KEY_USER_ID);
				json.remove(JK.KEY_USER_FACEBOOK_TOKEN);
				pstmt.setString(3, json.toString());

				pstmt.executeUpdate();
				db.updateQuizStats(quizID);
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				System.exit(-1);
			}

			JSONObject result = new JSONObject();
			result.put(JK.WEB_STATUS, "Done");
			return result;
		}
	}

	private void updateQuizStats(int quizId) throws SQLException {
		synchronized (lock) {
			QuizHolder quiz = new QuizHolder(getQuizData(quizId, true));
			{
				String sql = "SELECT * FROM quizResults WHERE quizId = ?;";
				PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, quizId);
				ResultSet rs = pstmt.executeQuery();

				while (rs.next()) {
					String resultsString = rs.getString("results");
					String reviewString = rs.getString("review");
					if (resultsString != null) {
						quiz.addResult(new JSONObject(resultsString));
					}
					if (reviewString != null) {
						quiz.addReview(new JSONObject(reviewString));
					}
				}
			}
			{
				updateQuizData(quizId,quiz.getJSON());
			}

		}

	}

	public JSONObject addQuizReview(JSONObject json) {
		synchronized (lock) {
			try {
				String sql;
				PreparedStatement pstmt;
				sql = "UPDATE quizResults SET review = ? WHERE quizId = ? AND userId = ?;";
				int quizID = json.getInt(JK.KEY_QUIZ_ID);
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(2, quizID);
				pstmt.setInt(3, json.getInt(JK.KEY_USER_ID));
				json.remove(JK.KEY_QUIZ_ID);
				json.remove(JK.KEY_USER_ID);
				json.remove(JK.KEY_USER_FACEBOOK_TOKEN);
				pstmt.setString(1, json.toString());

				pstmt.executeUpdate();
				db.updateQuizStats(quizID);
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				System.exit(-1);
			}

			JSONObject result = new JSONObject();
			result.put(JK.WEB_STATUS, "Done");
			return result;
		}
	}

	public boolean isUserIDTokenValid(int userId, String facebookToken) throws SQLException {
		synchronized (lock) {
			String sql = "SELECT name FROM users WHERE id = ? AND facebookToken = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, userId);
			pstmt.setString(2, facebookToken);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				return true;
			} else {
				return false;
			}
		}
	}
}
