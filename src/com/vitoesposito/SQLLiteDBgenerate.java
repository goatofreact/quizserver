package com.vitoesposito;

import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SQLLiteDBgenerate {

	public static void generateTestData() throws JSONException, SQLException {
		SQLLiteDB db = SQLLiteDB.get();
		{
			JSONObject user = generateUser("Vito", "tokenVito");
			db.updateUserData(user.getInt(JK.KEY_USER_ID), user);
		}
		{
			JSONObject user = generateUser("Lonnie", "tokenLonnie");
			db.updateUserData(user.getInt(JK.KEY_USER_ID), user);
		}
		{
			JSONObject user = generateUser("Chris", "tokenChris");
			db.updateUserData(user.getInt(JK.KEY_USER_ID), user);
		}

		{

			for (int title = 0; title < 5; title++) {
				JSONArray questions = new JSONArray();
				for (int i = 0; i < 10; i++) {
					JSONObject question = new JSONObject();
					question.put(JK.KEY_QUESTION_QUESTION, "Question " + (i + 1));
					question.put(JK.KEY_QUESTION_ANSWER_CORRECT, "Correct " + (i + 1));
					question.put(JK.KEY_QUESTION_ANSWER_WRONG1, "Wrong " + (i + 1) + "A");
					question.put(JK.KEY_QUESTION_ANSWER_WRONG2, "Wrong " + (i + 1) + "B");
					question.put(JK.KEY_QUESTION_ANSWER_WRONG3, "Wrong " + (i + 1) + "C");
					questions.put(question);
				}
				JSONObject quiz;
				if (title < 2) {
					quiz = generateQuiz("Title " + (title + 1), db.getUserIDFromUserName("Vito"), questions, false);
				} else {
					quiz = generateQuiz("Title " + (title + 1), db.getUserIDFromUserName("Lonnie"), questions, false);
				}
				db.updateQuizData(quiz.getInt(JK.KEY_QUIZ_ID), quiz);
			}
		}

		// Add Quiz Results
		{
			JSONObject json = generateQuizResults(0, 1, 2, 3, 0, 1, 2, 3, 2, 2);
			json.put(JK.KEY_USER_ID, db.getUserIDFromUserName("Vito"));
			json.put(JK.KEY_QUIZ_ID, 2);
			db.addQuizResults(json);
		}

	}

	static JSONObject generateQuizResults(int quizID, int... answers) {
		if (answers.length != 10) {
			int[] tempAnswers = new int[10];
			for (int i = 0; i < tempAnswers.length; i++) {
				tempAnswers[i] = 0;
			}
			for (int i = 0; i < answers.length && i < tempAnswers.length; i++) {
				tempAnswers[i] = answers[i];
			}
			answers = tempAnswers;
		}
		int[] durations = { 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000 };
		JSONObject json = new JSONObject();
		json.put(JK.KEY_QUIZ_ID, quizID);
		json.put(JK.KEY_QUIZ_ANSWERS, answers);
		json.put(JK.KEY_QUIZ_DURATIONS, durations);
		return json;
	}

	static JSONObject generateQuizStats(int quizId, int difficulty, int fun, int relevance, boolean isflagged) {
		JSONObject json = new JSONObject();
		json.put(JK.KEY_QUIZ_ID, quizId);
		json.put(JK.KEY_DIFFICULTY, difficulty);
		json.put(JK.KEY_FUN_FACTOR, fun);
		json.put(JK.KEY_RELEVANCE, relevance);
		json.put(JK.KEY_FLAGGED_COUNT, isflagged);
		return json;
	}

	static JSONObject generateUser(String name, String fbToken) throws JSONException, SQLException {
		SQLLiteDB db = SQLLiteDB.get();
		JSONObject user = new JSONObject();
		user.put(JK.KEY_USER_NAME, name);
		user.put(JK.KEY_USER_ID, db.getUserIDFromUserName(name));
		user.put(JK.KEY_USER_FACEBOOK_TOKEN, fbToken);
		JSONObject userStats = new JSONObject();
		user.put(JK.KEY_USER_STATS, userStats);
		return user;
	}

	static JSONObject generateQuiz(String title, int creator, JSONArray questions, boolean allowRandom)
			throws JSONException, SQLException {
		JSONObject quiz = new JSONObject();
		quiz.put(JK.KEY_QUIZ_TITLE, title);
		quiz.put(JK.KEY_QUIZ_ID, -1);
		quiz.put(JK.KEY_QUIZ_CREATOR_ID, creator);
		quiz.put(JK.KEY_QUIZ_QUESTIONS, questions);
		quiz.put(JK.KEY_QUIZ_ALLOW_RANDOM, allowRandom);

		quiz.put(JK.KEY_RELEVANCE, -1);
		quiz.put(JK.KEY_DIFFICULTY, -1);
		quiz.put(JK.KEY_FUN_FACTOR, -1);
		quiz.put(JK.KEY_FLAGGED_COUNT, 0);
		return quiz;
	}

	public static JSONObject generateQuizReview(int quizId, int difficulty, int funFactor, int relevance, int flagged) {
		JSONObject quiz = new JSONObject();
		quiz.put(JK.KEY_QUIZ_ID, quizId);
		quiz.put(JK.KEY_RELEVANCE, relevance);
		quiz.put(JK.KEY_DIFFICULTY, difficulty);
		quiz.put(JK.KEY_FUN_FACTOR, funFactor);
		quiz.put(JK.KEY_FLAGGED_COUNT, flagged);
		return quiz;
	}

}
