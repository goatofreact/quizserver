package com.vitoesposito;

import java.io.IOException;
import java.sql.SQLException;

import org.json.JSONException;

public class QuizProbeMain {
	static SingleInstance si = new SingleInstance("QuizServer");
	static SQLLiteDB db;

	public static void main(String[] args) throws IOException, InterruptedException, JSONException, SQLException {
		// https://certbot.eff.org/lets-encrypt/ubuntubionic-other
		// certbot -d bristol3.pki.enigmabridge.com --manual
		// --preferred-challenges dns certonly

		db = SQLLiteDB.get();
		WebServer web = null;
		try {
			web = new WebServer(62523);
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Starting");
		while (si.isAlive()) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Closing");
		if (web != null) {
			web.close();
		}
		si.close();
		System.out.println("Done");
		System.exit(0);
	}

}
