package com.vitoesposito;

import java.io.File;
import java.io.IOException;

public class SingleInstance {
	File runningFile;
	File killFile;
	String name;

	public SingleInstance(String name) {
		this.name = name;
		runningFile = new File(name + "Running.dat");
		killFile = new File(name + "Kill.dat");

		System.out.println(name + ": Starting instance");
		try {
			blockWhileOtherIsRunning();
			runningFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}

	}

	static final int MAX_WAIT = 5 * 60;
	static final int PRINT_DURATION = 10;
	static final int MAX_TICKS = MAX_WAIT / PRINT_DURATION;

	void blockWhileOtherIsRunning() throws IOException {
		if (runningFile.exists()) {
			System.out.println(name + ": Found other instance");
			killFile.createNewFile();
			for (int trys = 0; trys < MAX_TICKS; trys++) {
				for (int i = 0; i < PRINT_DURATION; i++) {
					if (!runningFile.exists()) {
						trys = MAX_TICKS;
						break;
					}
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				if (trys != MAX_TICKS) {
					System.out.println(name + ": Waiting: " + trys + "/" + MAX_TICKS);
				}
			}
			killFile.delete();
			System.out.println(name + ": Other instance gone starting now");
		}
	}

	public boolean isAlive() {
		if (killFile.exists()) {
			System.out.println(name + ": New instance requested");
			return false;
		} else {
			return true;
		}
	}

	public void close() {
		runningFile.delete();
		System.out.println(name + ": This instance closed");
	}
}
