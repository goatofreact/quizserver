package com.vitoesposito;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.sql.SQLException;
import java.util.concurrent.Executors;

import org.json.JSONException;
import org.json.JSONObject;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

public class WebServer {
	HttpServer server;

	public WebServer(int port) throws IOException {
		server = HttpServer.create(new InetSocketAddress(port), 100);
		HttpContext context = server.createContext("/");
		context.setHandler(WebServer::handleRequest);
		server.setExecutor(Executors.newFixedThreadPool(100));
		server.start();
	}

	static int requestCount = 0;

	private static void handleRequest(HttpExchange exchange) {
		String req = exchange.getRequestURI().toString();
		if (req.endsWith("/")) {
			req = req.substring(0, req.length() - 1);
		}
		String fromIP = exchange.getRemoteAddress().getAddress().getHostAddress();

		System.out.println();
		System.out.println("From: " + fromIP);
		System.out.println("Request " + (requestCount++) + ": " + req);
		if (fromIP.startsWith("192.168")) {
			fromIP = "68.104.63.51";
		}

		JSONObject response = new JSONObject();

		try {
			JSONObject post = readJSON(exchange.getRequestBody());
			response = WebServerEndPoints.login(post, false);
			if (response == null) {
				switch (req) {
				case "/login":
					response = WebServerEndPoints.login(post, true);
					break;
				case "/quizzes":
					response = WebServerEndPoints.getQuizzes(post);
					break;
				case "/quiz":
					response = WebServerEndPoints.getQuiz(post);
					break;
				case "/result":
					response = WebServerEndPoints.putResults(post);
					break;
				case "/review":
					response = WebServerEndPoints.putReview(post);
					break;
				case "/addQuiz":
					response = WebServerEndPoints.addQuiz(post);
					break;
				default:
					System.out.println("Unknown: " + req);
					break;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Response size:" + response.keySet().size());
		if (response == null || response.keySet().size() == 0) {
			response = new JSONObject();
			response.put(JK.WEB_STATUS, "FAIL");
		}
		String responseString = response.toString(2);
		System.out.println("Response: ");
		System.out.println(responseString);
		byte[] output = responseString.getBytes();

		try {
			exchange.sendResponseHeaders(200, output.length);
			OutputStream os = exchange.getResponseBody();
			os.write(output);
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		exchange.close();

	}

	public static JSONObject readJSON(InputStream is) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line;
		while ((line = in.readLine()) != null) {
			sb.append(line);
			if (line.endsWith("}")) {
				break;
			}
		}
		if (sb.length() > 0) {
			return new JSONObject(sb.toString());
		} else {
			return null;
		}
	}

	public void close() {
		server.stop(10);
	}

}
