package com.vitoesposito;

public class JK {
	static final String WEB_STATUS = "WebStatus";
	static final String WEB_ACTION = "Action";
	static final String WEB_GAME_LIST = "webGameList";
	static final String WEB_QUIZ_COMPLETED = "webQuizCompleted";
	static final String WEB_QUIZ_CREATOR = "webQuizCreator";

	static final String KEY_DIFFICULTY = "difficulty";
	static final String KEY_FUN_FACTOR = "funFactor";
	static final String KEY_RELEVANCE = "relevance";
	static final String KEY_FLAGGED_COUNT = "flagged";

	static final String KEY_QUIZ_ID = "quizId";
	static final String KEY_QUIZ_TITLE = "quizTitle";
	static final String KEY_QUIZ_CREATOR = "quizCreator";
	static final String KEY_QUIZ_CREATOR_ID = "quizCreatorId";
	static final String KEY_QUIZ_ALLOW_RANDOM = "quizAllowRandom";
	
	static final String KEY_QUIZ_QUESTIONS = "quizQuestions";
	static final String KEY_QUESTION_QUESTION = "questionQuestion";
	static final String KEY_QUESTION_ANSWER_CORRECT = "questionCorrect";
	static final String KEY_QUESTION_ANSWER_WRONG1 = "questionWrong1";
	static final String KEY_QUESTION_ANSWER_WRONG2 = "questionWrong2";
	static final String KEY_QUESTION_ANSWER_WRONG3 = "questionWrong3";
	

	

	static final String KEY_QUIZ_STATS = "quizStats";
	static final String KEY_QUIZ_RESULTS = "quizResults";
	static final String KEY_QUIZ_CORRECT_COUNT = "quizCorrectCount";
	static final String KEY_QUIZ_ANSWERS = "quizAnswers";
	static final String KEY_QUIZ_DURATIONS = "quizDurations";
	

	static final String KEY_USER_ID = "userId";
	static final String KEY_USER_NAME = "userName";
	static final String KEY_USER_FACEBOOK_TOKEN = "fbToken";
	static final String KEY_USER_STATS = "userStats";

}
